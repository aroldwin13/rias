<?php

namespace App\Models;

use App\Models\Contributor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Thesis extends Model
{
    use HasFactory;

    protected $table = 'thesis';
    protected $fillable = [
        'type',
    
    ];

}
