<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $table = 'stud_tb';
    protected $fillable = ['stud_id', 'surname', 'firstname'];

    
}
