<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Author;
use App\Models\Thesis;
use App\Models\Student;
use App\Models\Employee;
use App\Models\Contributor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class ContributorController extends Controller
{

    
    

    public function getStudents()
{  
    $students = Student::all();
    return response()->json($students);
}

    public function getEmployees(){

    $employees = Employee::all();
    return response()->json($employees);

    }

    public function create()
    {
        // $this->thesisId = $request->query('thesis_id');
        // Session::put('thesis_id', $this->thesisId);
       
            
        return Inertia::render('Create');
    }


    public function store(Request $request)
{
    // dd($request->all());
    $thesisId = Session::get('thesis_id');

    $typeAuthor = $request->input('typeAuthor');
    $typeAdviser = $request->input('typeAdviser');
    $typePanel = $request->input('typePanel');

    $authorData = $request->input('authorData');
    $adviserData = $request->input('adviserData');
    $panelData = $request->input('panelData');

    $tagsAuthor = $authorData['tags']['tags'];
   

    $tagsAdviser = $adviserData['tags']['tags'];
   

    $tagsPanel = $panelData['tags']['tags'];
   

    // Process Author contributors
    foreach ($tagsAuthor as $tag) {
        $personDetails = $tag['person'];
        if ($personDetails && is_array($personDetails)) {
            $contributor = new Contributor($request->all());
            $contributor->thesis_id = $thesisId;

            if ($typeAuthor == 1) {
                $contributor->type = $typeAuthor;
                $contributor->stud_id = $personDetails['id'];
            }
            $contributor->save();
        }
    }

    // Process Adviser contributors
    foreach ($tagsAdviser as $tag) {
        $personDetails = $tag['person'];
        if ($personDetails && is_array($personDetails)) {
            $contributor = new Contributor($request->all());
            $contributor->thesis_id = $thesisId;
            if ($typeAdviser == 2) {
                $contributor->type = $typeAdviser;
                $contributor->emp_id = $personDetails['id'];
            }
            $contributor->save();
        }
    }

    // Process Panel contributors
    foreach ($tagsPanel as $tag) {
        $personDetails = $tag['person'];
        if ($personDetails && is_array($personDetails)) {
            $contributor = new Contributor($request->all());
            $contributor->thesis_id = $thesisId;
            if ($typePanel == 3) {
                $contributor->type = $typePanel;
                $contributor->emp_id = $personDetails['id'];
            }
            $contributor->save();
        }
    }
    return redirect()->route('contributors.create');
}
        
  

            
        // $contributors = $request->input('contributors');
    
        // foreach ($contributors as $contributorData) {
            
        //     // Determine the contributor type
        //     $type = $contributorData['type'];
    
        //     // Create a new contributor based on the type
        //     if ($type == 1) {
        //         // If type is 1, it's a student
        //         Contributor::create([
        //             'thesis_id' => $thesisId,
        //             'type' => $type,
        //             'stud_id' => $contributorData['contributor'],
        //             // Add other fields as needed...
        //         ]);
        //     } elseif ($type == 2 || $type == 3) {
        //         // If type is 2 or 3, it's an employee
        //         Contributor::create([
        //             'thesis_id' => $thesisId,
        //             'type' => $type,
        //             'emp_id' => $contributorData['contributor'],
        //             // Add other fields as needed...
        //         ]);
        //     }
        // }
        
    }

