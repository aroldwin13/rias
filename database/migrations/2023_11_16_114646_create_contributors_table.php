<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributors', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('thesis_id');
            $table->tinyInteger('type')->comment('1=author
2=adviser
3=panel');
            $table->integer('emp_id')->nullable();
            $table->integer('stud_id')->unique();

        
        $table->foreign('stud_id')->references('id')->on('students');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributors');
    }
};
