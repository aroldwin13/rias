<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->tinyInteger('id', true);
            $table->integer('thesis_id')->nullable();
            $table->string('title', 100)->nullable();
            $table->string('series', 100)->nullable();
            $table->string('series_num', 100)->nullable();
            $table->string('volume', 100)->nullable();
            $table->integer('num_volume')->nullable();
            $table->string('edition', 100)->nullable();
            $table->string('place', 100)->nullable();
            $table->string('publisher', 100)->nullable();
            $table->string('date', 10)->nullable();
            $table->date('original_date')->nullable();
            $table->integer('num_pages')->nullable();
            $table->string('language', 100)->nullable();
            $table->string('isbn', 100)->nullable();
            $table->text('short_title')->nullable();
            $table->text('url')->nullable();
            $table->string('accessed', 100)->nullable();
            $table->string('archive', 100)->nullable();
            $table->string('loc_in_archive', 100)->nullable();
            $table->string('library_catalog', 100)->nullable();
            $table->string('call_number', 100)->nullable();
            $table->text('short_abstract')->nullable();
            $table->text('ext_abstract')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
};
